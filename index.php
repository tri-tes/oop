<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Tugas 9 | OOP</title>
</head>
<body>

</body>
</html>

<?php
require_once 'animal.php';
require_once 'ape.php';
require_once 'frog.php';

$sheep = new Animal('shaun');
echo "Name : ".$sheep->name." <br>";
echo "Legs : ".$sheep->legs." <br>";
echo "Cold Blooded : ".$sheep->cold_blooded." <br><br>";

$kodok = new Frog("buduk");
echo "Name : ".$kodok->name." <br>";
echo "Legs : ".$kodok->legs." <br>";
echo "Cold Blooded : ".$kodok->cold_blooded." <br>";
echo "Jump : ".$kodok->jump()."<br><br>";

$sungokong = new Ape("kera sakti");
echo "Name : ".$sungokong->name." <br>";
echo "Legs : ".$sungokong->legs." <br>";
echo "Cold Blooded : ".$sungokong->cold_blooded." <br>";
echo "Yell : ".$sungokong->yell()."<br>";

?>